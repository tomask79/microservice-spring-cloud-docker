package com.example.client.hystrix;

import com.example.client.feign.SimpleMicroServiceFeignClient;
import com.example.client.stubs.ClientPersonsTO;
import org.springframework.stereotype.Component;

/**
 * Created by tomask79 on 14.11.16.
 */
@Component
public class MicroServiceHystrixFallback implements SimpleMicroServiceFeignClient {
    @Override
    public String invokePersonsMicroService() {
        System.out.println("Waiting for circuit breaker to close!");
        return "";
    }
}
