# Writing MicroServices [part 4] #

## Spring Cloud MicroServices in Docker ##

In my previous posts about MicroServices ([part1](https://bitbucket.org/tomask79/microservices-spring-cloud), [part2](https://bitbucket.org/tomask79/microservice-spring-cloud-circuit-breaker) and [part3](https://bitbucket.org/tomask79/microservice-spring-cloud-feign)) I was always launching eureka server, services and client manually. Of course this isn't correct approach of starting Spring Cloud MicroServices.

Very popular way of launching the whole pack is to use [Docker](https://www.docker.com/) with [Docker Compose](https://docs.docker.com/compose/). If you are not familiar with Docker, I recommend to read my post regarding using [Docker with Spring MVC](https://bitbucket.org/tomask79/docker-spring-mvc).

Well, I won't be messing around with creating Docker compose configuration, because it's not that much of a problem. Real challenge lies somewhere else. But prerequisites first:

* Maven (building tool for everything)
* [Spotify docker maven plugin](https://github.com/spotify/docker-maven-plugin)
* Mac OS (Yosemite)
* Docker running via boot2docker 

Now to the basic approach, Spring Cloud MicroServices running in Docker containers can be accessed in the following common ways:

***From the outside of the Docker machine (docker machine IP)***

*Eureka and MicroServices are accessible through the static IP address of the docker machine*. Client will just fetch eureka registry and invoke the particular MicroService over docker-machine's IP and mapped container's port to the client's network port. 

***From the inside of the Docker machine***

*Eureka and MicroServices are running at the theirs container IP address inside of docker machine*. Client is also a Docker container application accessing Eureka and MicroService containers through [Docker links](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks/) or exposed container ports. Useful for **MicroService Gateways**.

Let's test first option. But to understand how to work with Spring Cloud in Docker, following needs to be clear:

**If you set eureka.instance.prefer-ip-address=true, then the MicroService registration with Eureka will instead use the IP address specified by eureka.instance.ip-address (which defaults to host's IP after resolving).
**

Hence **MicroService Spring Boot property file** is going to be:

```
.
.
eureka.client.serviceUrl.defaultZone=http://${DOCKER_HOST_IP}:9761/eureka
.
eureka.instance.preferIpAddress=true
eureka.instance.ip-address=${DOCKER_HOST_IP}
.
```
with this setting you're saying:

* Register this MicroService on Eureka running at: http://${DOCKER_HOST_IP}:9761/eureka
* MicroService is going to be exposed to the outside world of docker through the IP specified by the ${DOCKER_HOST_IP} system property. We're going to inject here the current IP address of docker machine.

Same meaning has this settings to **eureka server**.
```
.
.
eureka.instance.prefer-ip-address=true
eureka.instance.ip-address=${DOCKER_HOST_IP}
```
Eureka server instance is going to be running and accessible at ${DOCKER_HOST_IP}. 

## Docker maven integration ##

As Docker Maven plugin I choose [Spotify's Docker-Maven-Plugin](https://github.com/spotify/docker-maven-plugin). Fully functional, but **it doesn't support Docker Compose yet**, but we won't be using it anyway.

Configuration of this plugin and Dockerfile for Eureka and Services is more or less the same:

Eureka Dockerfile:
```
FROM java:8-jre
MAINTAINER Tomas Kloucek <tomas.kloucek@embedit.cz>

ADD ./demo-0.0.1-SNAPSHOT.war /app/

CMD ["java", "-Xmx512m", "-jar", "/app/demo-0.0.1-SNAPSHOT.war"]
```

Eureka pom.xml (plugin configuration)
```
.
.
			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.4.11</version>
				<configuration>
					<imageName>${docker.image.prefix}/${project.artifactId}</imageName>
					<dockerDirectory>src/docker</dockerDirectory>
					<resources>
						<resource>
							<targetPath>/</targetPath>
							<directory>${project.build.directory}</directory>
							<include>${project.build.finalName}.war</include>
						</resource>
					</resources>
				</configuration>
			</plugin>
```
to understand the parts, just read the [documentation](https://github.com/spotify/docker-maven-plugin).

## Building Docker images and running them in containers ##

Eureka:
```
(in the spring-microservice-registry folder)
mvn clean install docker:build
docker images (verify your image was built)
docker run --env DOCKER_HOST_IP=$(boot2docker ip) -it --name eureka -p 9761:9761 registry/demo
```
(visit http://DOCKER_HOST_IP:9761/ to verify that eureka is running)

Service:
```
(in the spring-microservice-service folder)
mvn clean install -DskipTests docker:build
docker images (verify your image was built)
docker run --env DOCKER_HOST_IP=$(boot2docker ip) -it --name service -p 8080:8080 service/demo
docker ps (verify that service container is running)
```

## Invoking the MicroService from the outside ##

Outside client just needs to know address of eureka server

application.properties
```
.
.
eureka.client.serviceUrl.defaultZone=http://${DOCKER_HOST_IP}:9761/eureka
.
.
```

*Testing the client against the dockerized MicroService*
```
(in the spring-microservice-client folder)
mvn clean install
java -jar -DDOCKER_HOST_IP=$(boot2docker ip) target/demo-0.0.1-SNAPSHOT.war 
```

result should look like (repeated output):

Simple MicroService Feign invocation result :{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}

That's all!
There are many other ways of how to make the docker container to see at the other one:

* Docker Links
* Over Docker network
* Deploying into Docker Swarm cluster

I will test this next time.

regards

Tomas